module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),
    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd HH:MM") %> */\n'
      },
      p2m: {
        files: {
          'public/p2m.js': ['public/p2m.dev.js']
        }
      },
      p2m_process: {
        files: {
          'public/p2m_process.min.js': ['public/p2m_process.js']
        }
      },
      socketio: {
        files: {
          'public/socketio.min.js': ['public/socket.io.js']
        }
      }
    }
  });

  // Load the plugin that provides the "uglify" task.
  grunt.loadNpmTasks('grunt-contrib-uglify');

  // Default task(s).
  grunt.registerTask('default', ['uglify']);

};
