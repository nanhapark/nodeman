/**
 * Play node.js
 * Websocket Service #P2M
 *
 * @author nanhapark
 * @date 2013-04
 */
(function() {
  var prefix = 'websocket p2m service :: ';

  if (typeof jQuery == 'undefined') { alert(prefix + 'not found jQuery'); return }

  var $ = jQuery;

  $('#socketio').remove();

  //
  // google chart api
  //
  var GOOGLE_CHART_URL = 'http://chart.apis.google.com/chart';
  var GOOGLE_QRCODE_URL = GOOGLE_CHART_URL + '?cht=qr';

  //
  // core url
  // 포트를 지정하지 않으면, 브라우져 포트를 따라감.
  //
  var URL = 'http://socket.nodeman.org:80';
  var SOCKETIO_URL = URL + '/socketio.min.js';
v

  //
  // target id
  //
  var TARGET_ID = 'socket_nodeman_qrcode';

  //
  // good image
  //
  var GOOD_IMG = window.nodeman.close_img || 'http://nodeman.org/images/good.png';

  //
  // load websocket
  //
  $.getScript(SOCKETIO_URL, callbackSocketIO);
  
  //
  // jquery base64
  //
  jQuery.base64 = (function($) {
    var _PADCHAR = "=",
      _ALPHA = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/",
      _VERSION = "1.0";

    function _getbyte64( s, i ) {
      var idx = _ALPHA.indexOf( s.charAt( i ) );
      if ( idx === -1 ) {
        throw "Cannot decode base64";
      }
      return idx;
    }
    
    function _decode( s ) {
      var pads = 0,
        i,
        b10,
        imax = s.length,
        x = [];

      s = String( s );
      
      if ( imax === 0 ) {
        return s;
      }

      if ( imax % 4 !== 0 ) {
        throw "Cannot decode base64";
      }

      if ( s.charAt( imax - 1 ) === _PADCHAR ) {
        pads = 1;
        if ( s.charAt( imax - 2 ) === _PADCHAR ) {
          pads = 2;
        }
        imax -= 4;
      }

      for ( i = 0; i < imax; i += 4 ) {
        b10 = ( _getbyte64( s, i ) << 18 ) | ( _getbyte64( s, i + 1 ) << 12 ) | ( _getbyte64( s, i + 2 ) << 6 ) | _getbyte64( s, i + 3 );
        x.push( String.fromCharCode( b10 >> 16, ( b10 >> 8 ) & 0xff, b10 & 0xff ) );
      }

      switch ( pads ) {
        case 1:
          b10 = ( _getbyte64( s, i ) << 18 ) | ( _getbyte64( s, i + 1 ) << 12 ) | ( _getbyte64( s, i + 2 ) << 6 );
          x.push( String.fromCharCode( b10 >> 16, ( b10 >> 8 ) & 0xff ) );
          break;
        case 2:
          b10 = ( _getbyte64( s, i ) << 18) | ( _getbyte64( s, i + 1 ) << 12 );
          x.push( String.fromCharCode( b10 >> 16 ) );
          break;
      }
      return x.join( "" );
    }
    
    function _getbyte( s, i ) {
      var x = s.charCodeAt( i );
      if ( x > 255 ) {
        throw "INVALID_CHARACTER_ERR: DOM Exception 5";
      }
      return x;
    }

    function _encode( s ) {
      if ( arguments.length !== 1 ) {
        throw "SyntaxError: exactly one argument required";
      }

      s = String( s );

      var i,
        b10,
        x = [],
        imax = s.length - s.length % 3;

      if ( s.length === 0 ) {
        return s;
      }

      for ( i = 0; i < imax; i += 3 ) {
        b10 = ( _getbyte( s, i ) << 16 ) | ( _getbyte( s, i + 1 ) << 8 ) | _getbyte( s, i + 2 );
        x.push( _ALPHA.charAt( b10 >> 18 ) );
        x.push( _ALPHA.charAt( ( b10 >> 12 ) & 0x3F ) );
        x.push( _ALPHA.charAt( ( b10 >> 6 ) & 0x3f ) );
        x.push( _ALPHA.charAt( b10 & 0x3f ) );
      }

      switch ( s.length - imax ) {
        case 1:
          b10 = _getbyte( s, i ) << 16;
          x.push( _ALPHA.charAt( b10 >> 18 ) + _ALPHA.charAt( ( b10 >> 12 ) & 0x3F ) + _PADCHAR + _PADCHAR );
          break;
        case 2:
          b10 = ( _getbyte( s, i ) << 16 ) | ( _getbyte( s, i + 1 ) << 8 );
          x.push( _ALPHA.charAt( b10 >> 18 ) + _ALPHA.charAt( ( b10 >> 12 ) & 0x3F ) + _ALPHA.charAt( ( b10 >> 6 ) & 0x3f ) + _PADCHAR );
          break;
      }

      return x.join( "" );
    }

    return {
      decode: _decode,
      encode: _encode,
      VERSION: _VERSION
    };
  }(jQuery));

  //
  // express qrcode
  //
  var QRCode = {
    remove: function() {
      $('#qrcode').fadeOut(1000, function() { $(this).remove() });
    },
    append: function(url) {
      this.remove();

      var $target = $('#' + TARGET_ID);
      if ($target.length > 0) {
        var w = $target.width() || 230, h = $target.height() || 230;
        var a = GOOGLE_QRCODE_URL + '&chs=' + w + 'x' + h + '&chl=' + encodeURIComponent(url) + '&chld=H|0';
        $('<div></div>').attr({id: 'qrcode'}).css({
          boxShadow: '0px 2px 20px rgba(0, 0, 0, 1)',
        }).html('<img src="' + a + '" border="0">').appendTo($target).hide().fadeIn(1000);
      } else {
        var a = GOOGLE_QRCODE_URL + '&chs=230x230&chl=' + encodeURIComponent(url) + '&chld=H|0';
        $('<div></div>').attr({id: 'qrcode'}).css({
          position: 'absolute',
          top : ($(window).scrollTop() + 10) + 'px',
          left: '10px',
          boxShadow: '0px 2px 20px rgba(0, 0, 0, 1)',
          zIndex: 1000000
        }).html('<img src="' + a + '" border="0">').appendTo(document.body).hide().fadeIn(1000);
      }

      //
      // invoke click event
      //
      $('#qrcode').click(function(e) {
        if (typeof window.nodeman.onclick == 'function') {
          window.nodeman.onclick.call(this);
        } else {
          $(this).remove();
        }
      });
    }
  };

  //
  // re-action from websocket
  //
  var E = {
    close: function() {
      var W = $('#qrcode').width();
      var H = $('#qrcode').height();
      $('#qrcode').animate({  borderSpacing: -360 }, {
          step: function(now,fx) {
            $(this).css('-webkit-transform','rotate('+now+'deg)');
            $(this).css('-moz-transform','rotate('+now+'deg)'); 
            $(this).css('-ms-transform','rotate('+now+'deg)');
            $(this).css('-o-transform','rotate('+now+'deg)');
            $(this).css('transform','rotate('+now+'deg)');  
          },
          duration:'slow',
          complete: function() {
            $('<img src="' + GOOD_IMG + '" style="position: absolute; top: 0; left:0; opacity: 0.9; z-Index: 1000" width="' + W + '" height="' + H + '">').appendTo('#qrcode').hide().fadeIn('slow', function() {
              setTimeout(function() {
                $('#qrcode img').fadeOut(1000, function() { $(this).remove() })
              }, window.nodeman.close_timeout || 10 * 1000);
            });
          }
      }, 'linear');

      //
      // execute callback
      //
      if (typeof window.nodeman.callback == 'function') {
        window.nodeman.callback.call(this);
      }
    }
  };

  //
  // handle websocket connect
  //
  function handleConnect() {
    var suffix = '',
        str    = (window.getSelection) ? window.getSelection() : document.selection.createRange(),
        pattern, url, u;

    str = str.text || str;
    str = (str + '').replace(/-/g, ''); // the best way to make object a string...
    pattern = /^\s*\d+\s*$/;
    if (pattern.test(str)) {
      suffix = '&tel=' + str;
    }

    url = $.base64.encode(window.nodeman.url || document.location.href);
    u = URL + '/p2m?sessid=' + this.socket.sessionid + '&url=' + url + suffix;

    QRCode.append(u);
  }

  //
  // handle re-action from websocket
  //
  function handleReturn(data) {
    console.log(prefix + data);
    if (!E[data]) {
      alert(prefix + 'action not found');
      return;
    }
    E[data]()
  }

  //
  // callback get script socket.io file
  //
  function callbackSocketIO() {
    console.log(prefix + 'io load complete');

    //
    // connect websocket
    //
    var s = io.connect(URL);

    //
    // re-action from websocket
    //
    s.on('return', handleReturn);
    
    //
    // show qrcode after received socket sessionid
    //
    s.on('connect', handleConnect);
  }
})();
