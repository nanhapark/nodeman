
/*
 * GET home page.
 */

exports.index = function(req, res){
  res.render('index');
};
exports.intro_navigator = function(req, res){
  res.render('intro_navigator');
};
exports.intro_p2m = function(req, res){
  res.render('intro_p2m');
};
exports.control = function(req, res){
  var sessid = req.params.sessid;
  res.render('control', {
    sessid: sessid
  });
};
exports.p2m = function(req, res){
  log.info(req.query);
  var sessid = req.query.sessid || '';
  var url = req.query.url || '';
  var tel = req.query.tel || '';
  res.render('p2m', {
    sessid: sessid,
    url: new Buffer(url, 'base64').toString('ascii'),
    tel: tel
  });
};
